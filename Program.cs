﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace PunchcardReader
{
    class Program
    {
        private static Dictionary<bool[], char> CharacterLookup { get; set; }

        private static List<bool[][]> ProcessedImages { get; set; }

        // Some punchcard-image layout constants
        const int startX = 16; // First punch-hole's position in pixels
        const int startY = 20;
        const int pitchX = 7;  // Pitch between punch-holes in pixels (hole-size + space to next)
        const int pitchY = 20;
        const int countX = 80; // Number of punch-holes on card
        const int countY = 12;

        static void Main( string[] args )
        {
            ProcessedImages = new List < bool[][] >();

            foreach ( var a in args )
            {
                Console.WriteLine( a );

                if ( Path.GetFileName( a ).Contains( "key" ) )
                {
                    ProcessCharacterKeyImage( a );
                    continue;
                }

                ProcessedImages.Add( ProcessImage( a ) );
            }

            Decode();

            Console.ReadLine();
        }

        private static void Decode()
        {
            if ( CharacterLookup == null )
            {
                Console.WriteLine("Did not get key file...");
                return;
            }

            string output = String.Empty;

            foreach ( var image in ProcessedImages )
            {
                foreach ( var column in image )
                {
                    var ch = ' ';
                    
                    if ( CharacterLookup.ContainsKey( column ) )
                        ch = CharacterLookup[column];

                    output += ch;
                }
            }

            Console.WriteLine(output);
        }


        private static void ProcessCharacterKeyImage( string imagePath )
        {
            // Taken from the "key" punchcard
            var keyCharacters = @"&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'=""[.<(+^!$*);\],%_>?";

            var contents = ProcessImage( imagePath );

            CharacterLookup = new Dictionary < bool[], char >( new BoolArrayEqualityComparer() );

            for ( int i = 0; i < keyCharacters.Length; i++)
            {
                if (!CharacterLookup.ContainsKey( contents[i] ))
                    CharacterLookup.Add( contents[i], keyCharacters[i] );
            }
        }

        private static bool[][] ProcessImage( string imagePath )
        {
            Bitmap image = new Bitmap(imagePath);

            var imageData = new bool[countX][];
            for (int i = 0; i < countX; i++)
                imageData[i] = new bool[countY];

            for (int x = 0; x < countX; x++)
            {
                var pixelX = startX + x * pitchX;

                for (int y = 0; y < countY; y++)
                {
                    var pixelY = startY + y * pitchY;

                    var pixel = image.GetPixel(pixelX, pixelY);

                    var isBlack = pixel.R < 40 && pixel.G < 40 && pixel.B < 40;

                    imageData[x][y] = isBlack;
                }

            }

            //PrintImageData(imageData);

            return imageData;
        }

        private static void PrintImageData(bool[][] contents)
        {
            var output = new StringBuilder();

            for (int y = 0; y < countY; y++)
            {
                for (int x = 0; x < countX; x++)
                {
                    output.Append(contents[x][y] ? "*" : "_");
                }

                output.Append(Environment.NewLine);
            }

            Console.WriteLine(output.ToString());
        }
    }

    public class BoolArrayEqualityComparer : IEqualityComparer<bool[]>
    {
        public bool Equals( bool[] x, bool[] y )
        {
            if ( x.Length != y.Length )
            {
                return false;
            }
            for ( int i = 0; i < x.Length; i++ )
            {
                if ( x[i] != y[i] )
                {
                    return false;
                }
            }
            return true;
        }

        public int GetHashCode( bool[] obj )
        {
            int result = 17;
            for ( int i = 0; i < obj.Length; i++ )
            {
                unchecked
                {
                    result = result * 23 + (obj[i] ? 1 : 3);
                }
            }
            return result;
        }
    }
}
