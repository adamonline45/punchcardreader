# Purpose
This program was written for a CTF hacking competition, The Boston Key Party, held in 2015. In CTF competitions, the goal is to solve a variety of puzzles and submit the revealed keys to the judges for points.

In this challenge (called "Haymarket"), a set of Hollerith punchcards was provided, which contained a COBOL program. Afted decoding the provided cards using this program, the key was found being assigned to a variable named "KEY" and was submitted, **winning our team the flag for that challenge!**

# The Program
This program processes the punchcard images and turns them into text.

After building, drag and drop punchcard image files onto the executable. There must be one with the term "key" in its filename which the program uses to map punchcard inputs to their corresponding characters.

# Notes
Sample punchcard images can be found in /cards/ or make your own at http://www.kloth.net/services/cardpunch.php . The sample card image "/cards/answer.png" can be parsed to see the winning key for this challenge!

Please note this was built in a great hurry, to yield one result and be forgotten about forever! It may have issues... ;)